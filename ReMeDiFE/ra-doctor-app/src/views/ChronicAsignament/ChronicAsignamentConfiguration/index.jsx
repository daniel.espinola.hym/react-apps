import React from 'react';
import { Grid } from '@material-ui/core';
//self components
import DiagnosisChronicTable from './DiagnosisChronicTable';
import ChronicAssignmentHistoryTrable from './ChronicAssignmentHistoryTable';
import DrugTableModal from './DrugTableModal';

const ChronicAsignamentConfiguration = ({
  patientChronicDiagnosisList,
  chronicAssignmentPatientList,
  handleOnChronicDiagnosisSelect,
  deleteChronicDiagnosisSelected,
  edit,
  handleOnShowDrugInfoSelect,
  drugList,
  showDrugModal,
  handleOnClose,
  titleChronicAssignment
}) => {
  return (
    <Grid container>
      <Grid item md={12} xs={12}>
        <DiagnosisChronicTable
          patientChronicDiagnosisList={patientChronicDiagnosisList}
          onChronicDiagnosisSelect={handleOnChronicDiagnosisSelect}
          deleteChronicDiagnosisSelected={deleteChronicDiagnosisSelected}
          edit={edit}
          titleChronicAssignment={titleChronicAssignment}></DiagnosisChronicTable>
      </Grid>
      {!edit && (
        <Grid item md={12} xs={12}>
          <br></br>
          <ChronicAssignmentHistoryTrable
            chronicAssignmentPatientList={chronicAssignmentPatientList}
            handleOnShowDrugInfoSelect={
              handleOnShowDrugInfoSelect
            }></ChronicAssignmentHistoryTrable>
          <DrugTableModal
            drugList={drugList} // LA LISTA DE DROGAS
            isOpen={showDrugModal} // SI EL MODAL ESTA ABIERTO
            onClose={handleOnClose} // CIERRO EL MODAL
          />
        </Grid>
      )}
    </Grid>
  );
};

export default ChronicAsignamentConfiguration;
