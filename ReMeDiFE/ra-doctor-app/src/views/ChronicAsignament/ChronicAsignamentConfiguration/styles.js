export default theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  content: {
    marginTop: theme.spacing.unit * 2
  },
  progressWrapper: {
    paddingTop: '48px',
    paddingBottom: '24px',
    display: 'flex',
    justifyContent: 'center'
  },
  tableRow: {
    height: '64px'
  },
  tableCell: {
    whiteSpace: 'normal'
  },
  tableCellInner: {
    display: 'flex',
    alignItems: 'center'
  },
  prescriptionContainer: {
    minHeight: '500px'
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    display: 'inline-flex',
    fontSize: '14px',
    fontWeight: 500,
    height: '36px',
    width: '36px'
  },
  nameText: {
    display: 'inline-block',
    marginLeft: theme.spacing.unit * 2,
    fontWeight: 500,
    cursor: 'pointer'
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  group: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2
  },
  groupLabel: {
    paddingLeft: theme.spacing.unit * 2
  },
  field: {
    marginBottom: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center'
  },
  textField: {
    width: '320px',
    maxWidth: '100%',
    marginRight: theme.spacing.unit * 3
  },
  textFieldTable: {
    width: '100px'
  }
});
