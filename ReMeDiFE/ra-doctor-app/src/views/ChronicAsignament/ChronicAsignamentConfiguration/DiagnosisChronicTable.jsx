import React, { useState } from 'react';
import classNames from 'classnames';
import styles from './styles';
import {
  Portlet,
  PortletContent,
  PortletLabel,
  PortletHeader
} from 'components';
import { Create } from '@material-ui/icons';
import { AddCircle } from '@material-ui/icons';
import PostAddIcon from '@material-ui/icons/PostAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles
} from '@material-ui/core';


const DiagnosisChronicTable = ({
  className,
  classes,
  patientChronicDiagnosisList,
  onChronicDiagnosisSelect,
  deleteChronicDiagnosisSelected,
  edit,
  titleChronicAssignment
}) => {
  //var [titleDiagnosisTable, setTitleDiagnosisTable] = useState("Seleccione el diagnóstico para el cual quiere realizar la asignación");
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet className={rootClassName}>
      <PortletHeader>
        <PortletLabel title={titleChronicAssignment} />
      </PortletHeader>
      <PortletContent noPadding>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Fecha</TableCell>
              <TableCell align="left">Diagnóstico</TableCell>
              <TableCell align="left">Doctor</TableCell>
              <TableCell align="left">Hospital</TableCell>
              <TableCell align="left">Acción</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {patientChronicDiagnosisList.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.tableCell} colSpan={5}>
                  No hay datos
                </TableCell>
              </TableRow>
            )}
            {patientChronicDiagnosisList.map(pcd => {
              return (
                <React.Fragment>
                  <TableRow className={classes.tableRow} hover key={pcd.id}>
                    <TableCell className={classes.tableCell}>
                      {pcd.insertDateString}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {pcd.chronic_disease_id.diagnosis_id.description}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {pcd.doctor.fullName}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {pcd.hospital.name}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {!edit && (
                        <PostAddIcon
                          style={{ cursor: 'pointer' , color: '#008388'}}
                          onClick={() => {onChronicDiagnosisSelect(pcd.id);}}
                        />
                      )}
                      {edit && (
                        <DeleteIcon
                          style={{ cursor: 'pointer' }}
                          onClick={() => {deleteChronicDiagnosisSelected();}}
                        />
                      )}
                    </TableCell>
                  </TableRow>
                </React.Fragment>
              );
            })}
          </TableBody>
        </Table>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(DiagnosisChronicTable);
