import React from 'react';
import classNames from 'classnames';
import {
  Portlet,
  PortletContent,
  PortletHeader,
  PortletLabel
} from 'components';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles
} from '@material-ui/core';
import styles from './styles';

import { SearchOutlined } from '@material-ui/icons';

const ChronicAssignmentHistoryTable = ({
  className,
  classes,
  chronicAssignmentPatientList,
  handleOnShowDrugInfoSelect
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Configuraciones generadas" />
      </PortletHeader>
      <PortletContent noPadding>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Fecha</TableCell>
              <TableCell align="left">Diagnóstico</TableCell>
              <TableCell align="left">Doctor</TableCell>
              <TableCell align="left">Hospital</TableCell>
              <TableCell align="left">Recetas generadas</TableCell>
              <TableCell align="left">Frecuencia</TableCell>
              <TableCell align="left">Acción</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {chronicAssignmentPatientList.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.tableCell} colSpan={7}>
                  No hay datos
                </TableCell>
              </TableRow>
            )}
            {chronicAssignmentPatientList.map(cap => {
              return (
                <React.Fragment>
                  <TableRow className={classes.tableRow} hover key={cap.id}>
                    <TableCell className={classes.tableCell}>
                      {cap.starterDate}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {cap.diagnosis}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {cap.doctorName}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {cap.hospitalName}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {cap.triggeredQuantity}/{cap.quantity}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      {cap.frequency}
                    </TableCell>
                    <TableCell className={classes.tableCell}>
                      <SearchOutlined
                        style={{ cursor: 'pointer' }}
                        onClick={() =>
                          handleOnShowDrugInfoSelect(cap.prescriptionId, cap.id)
                        }
                      />
                    </TableCell>
                  </TableRow>
                </React.Fragment>
              );
            })}
          </TableBody>
        </Table>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(ChronicAssignmentHistoryTable);
