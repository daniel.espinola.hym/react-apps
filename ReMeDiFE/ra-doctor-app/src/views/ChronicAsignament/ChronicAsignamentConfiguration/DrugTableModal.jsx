import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

const createStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  tableRow: {
    height: '64px'
  },
  tableCell: {
    whiteSpace: 'normal'
  },
  tableCellInner: {
    display: 'flex',
    alignItems: 'center'
  }
}));

const DrugTableModal = React.memo(({ onClose, drugList, isOpen }) => {
  const classes = createStyles();

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
      maxWidth="md"
      fullWidth>
      <DialogTitle id="form-dialog-title">Lista de medicamentos</DialogTitle>
      <DialogContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Nombre</TableCell>
              <TableCell align="left">Composición</TableCell>
              <TableCell align="left">Concentración</TableCell>
              <TableCell align="left">Forma Farmacéutica</TableCell>
              <TableCell align="left">Días</TableCell>
              <TableCell align="left">Posología</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {drugList.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.tableCell} colSpan={6}>
                  No hay medicamentos
                </TableCell>
              </TableRow>
            )}
            {drugList.map(drug => {
              if (!drug.comercial) {
                //es droga generica
                return (
                  <React.Fragment>
                    <TableRow className={classes.tableRow} hover key={drug.id}>
                      <TableCell className={classes.tableCell}>
                        {drug.name}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <ul>-</ul>
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.strength}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.pharmaceuticalForm}
                      </TableCell>
                      <TableCell className={classes.tableCell} size="small">
                        {drug.days}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.treatment}
                      </TableCell>
                    </TableRow>
                  </React.Fragment>
                );
              } else {
                //es droga comercial
                return (
                  <React.Fragment>
                    <TableRow className={classes.tableRow} hover key={drug.id}>
                      <TableCell className={classes.tableCell}>
                        {drug.name}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <ul>
                          {drug.composition.map(composition => (
                            <li>{composition.description}</li>
                          ))}
                        </ul>
                      </TableCell>
                      <TableCell className={classes.tableCell}>-</TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.pharmaceuticalForm}
                      </TableCell>
                      <TableCell className={classes.tableCell} size="small">
                        {drug.days}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.treatment}
                      </TableCell>
                    </TableRow>
                  </React.Fragment>
                );
              }
            })}
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Cerrar
        </Button>
      </DialogActions>
    </Dialog>
  );
});

export default DrugTableModal;
