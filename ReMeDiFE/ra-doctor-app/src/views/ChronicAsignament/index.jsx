import React, { Component } from 'react';
import { Grid, Button, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { AccessAlarm } from '@material-ui/icons';
import {
  Portlet,
  PortletContent,
  PortletHeader,
  PortletLabel
} from 'components';

//self components
import PatientSearch from '../../components/PatientSearch';
import DoctorInfo from '../../components/DoctorInfo';
import PatientInfo from '../../components/PatientInfo';
import ChronicAsignamentConfiguration from './ChronicAsignamentConfiguration';
import ChronicPrescription from './ChronicPrescription';

//external components
import { Dashboard as DashboardLayout } from 'layouts';
import LoadingScreen from 'react-loading-screen';

//APIS
import { getAllChronicDiagnosisByPatientId } from 'api/diagnosis';
import { getPatientByDni } from 'api/patient';
import { getAllDrugsBack, getDrugListByPrescriptionId } from 'api/drug';
import {
  postChronicAssignment,
  getChronicAssignmentByPatientId
} from 'api/medicalconsultation';
import { getDoctorInformation } from 'api/doctor';

import Modal from 'react-bootstrap/Modal';
import ButtonBootstrap from 'react-bootstrap/Button';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  content: {
    marginTop: theme.spacing.unit * 2
  },
  field: {
    margin: theme.spacing.unit * 3
  },
  progressWrapper: {
    paddingTop: '48px',
    paddingBottom: '24px',
    display: 'flex',
    justifyContent: 'center'
  },
  textField: {
    width: '150px',
    maxWidth: '100%',
    marginRight: theme.spacing.unit * 3
  }
});

const frequencies = [
  {
    value: 'm',
    label: 'Minutos'
  },
  {
    value: 'q',
    label: 'Quincenal'
  },
  {
    value: 's',
    label: 'Semanal'
  },
  {
    value: 'm',
    label: 'Mensual'
  }
];

class ChronicAsignament extends Component {
  constructor() {
    super();
    this.state = {
      chronicAssignmentObject: {
        drugList: [],
        chronicDiagnosis: 0,
        quantity: 0,
        starterDate: '',
        frequency: ''
      },
      doctorInfo: {},
      patientInfo: {},
      loading: false,
      dniSelected: false,
      diseaseSelected: false,
      showDrugModal: false,
      quantity: 0,
      starterDate: '',
      frequency: 'm',
      patientDiseaseSelected: [],
      patientChronicDiagnosisList: [],
      patientChronicAssignmentList: [],
      drugList: [],
      drugDataTable: [],
      drugListObject: [],
      selectedDrugs: [],
      drugModalList: [],
      showModal: false,
      messageModal: '',
      modalTitle: '',
      titleChronicAssignment: "Seleccione el diagnóstico para el cual quiere realizar la asignación",
      modalVariant: ''
    };
  }

  handlePatientClickSearch = async dni => {
    try {
      const r = await getPatientByDni(parseInt(dni));

      if (r.data.error != null) {
        // this.setState({ loading: false});
        this.setState({ modalVariant: "danger" });
        this.setState({ showModal: true });
        this.setState({ messageModal: r.data.error });
        this.setState({ modalTitle: "Error" });
      } else {
        this.setState({ modalVariant: "info" });
        let patient = {};
        patient.dni = r.data.response.dni;
        patient.name = r.data.response.fullName;
        patient.age = r.data.response.age;
        patient.id = r.data.response.id;
        const doctorInfoView = await getDoctorInformation();
        this.setState({doctorInfo: doctorInfoView});
        const patientChronicDiagnosisRequest = await getAllChronicDiagnosisByPatientId(patient.id);
        const drugPromise = await getAllDrugsBack();
        const chronicAssignmentPatientPromise = await getChronicAssignmentByPatientId(r.data.response.id);
        const drugGenericList = drugPromise.data.response.drugGenericList;
        const drugComercialList = drugPromise.data.response.drugComercialList;
        const drugList = [];

        drugGenericList.forEach(drugGeneric => {
          drugList.push(drugGeneric);
        });

        drugComercialList.forEach(drugComercial => {
          drugList.push(drugComercial);
        });

        this.setState({
          patientChronicDiagnosisList: patientChronicDiagnosisRequest.data.response,
          drugList: drugList,
          patientInfo: patient,
          patientChronicAssignmentList: chronicAssignmentPatientPromise.data.response,
          dniSelected: true
        });

      }
    } catch (ex) {
      alert("Error interno")
      console.log(ex);
    }
  };

  handleOnChronicDiagnosisSelect = id => {
    var pSelected = [];
    pSelected.push(this.state.patientChronicDiagnosisList.find(x => x.id === id));
    this.setState({ diseaseSelected: true, patientDiseaseSelected: pSelected });
    this.setState({ titleChronicAssignment: "Diagnóstico seleccionado" });
  };

  handleOnChronicDiagnosisDelete = () => {
    var pSelected = [];
    this.setState({ diseaseSelected: false, patientDiseaseSelected: pSelected });
    this.setState({ titleChronicAssignment: "Seleccione el diagnóstico para el cual quiere realizar la asignación" });    
  };

  //drugs components functions
  handleDrugAdd = async selectedDrugObject => {
    const { selectedDrugs, drugListObject, drugDataTable } = this.state;

    if (selectedDrugs.includes(selectedDrugObject.id)) {
      return;
    } else {
      selectedDrugs.push(selectedDrugObject.id);
    }

    const drugObject = {
      id: selectedDrugObject.id,
      name: selectedDrugObject.name,
      days: selectedDrugObject.days,
      strength: selectedDrugObject.strength,
      pharmaceuticalForm: selectedDrugObject.pharmaceuticalForm,
      treatment: selectedDrugObject.treatment
    };

    if (selectedDrugObject.isComercial) {
      const drugTableObject = {
        id: selectedDrugObject.id,
        name: selectedDrugObject.name,
        days: selectedDrugObject.days,
        composition: selectedDrugObject.composition,
        pharmaceuticalForm: selectedDrugObject.pharmaceuticalFormDescription,
        treatment: selectedDrugObject.treatment,
        isComercial: true
      };

      this.setState({
        drugListObject: [...drugListObject, drugObject],
        drugDataTable: [...drugDataTable, drugTableObject]
      });
    } else {
      const drugTableObject = {
        id: selectedDrugObject.id,
        name: selectedDrugObject.name,
        days: selectedDrugObject.days,
        strength: selectedDrugObject.strengthDescription,
        pharmaceuticalForm: selectedDrugObject.pharmaceuticalFormDescription,
        treatment: selectedDrugObject.treatment,
        isComercial: false
      };

      this.setState({
        drugListObject: [...drugListObject, drugObject],
        drugDataTable: [...drugDataTable, drugTableObject]
      });
    }
  };

  handleDrugDelete = drugId => {
    this.setState({
      selectedDrugs: this.state.selectedDrugs.filter(d => d !== drugId)
    });
    this.setState({
      drugListObject: this.state.drugListObject.filter(d => d.id !== drugId)
    });
    this.setState({
      drugDataTable: this.state.drugDataTable.filter(d => d.id !== drugId)
    });
  };

  //fin drugs

  saveChronicAssignment = async () => {
    let data = {};
    data.prescription = {};
    data.prescription.drugList = this.state.drugListObject;
    data.hospitalId = this.state.doctorInfo.hospitalId;
    data.doctorId = this.state.doctorInfo.id;
    data.patientChronicDiseaseId = this.state.patientDiseaseSelected[0].id;
    data.quantity = this.state.quantity;
    data.frequency = this.state.frequency;
    const assignmentChronicRequest = postChronicAssignment(data);
    console.log('tu data es:');
    console.log(data);
    console.log('el estado es:');
    console.log(this.state);
    console.log(assignmentChronicRequest);
    this.showSuccessModalMessage('La operación terminó con éxito');
    //alert('La operación concluyó con éxito');
    // this.setState({ dniSelected: false, diseaseSelected: false });
    //asd
  };

  handleModalClose = () => {
    this.setState({ showModal: false, messageModal: '' });

    this.setState({patientDiseaseSelected: []});
    this.setState({patientChronicDiagnosisList: []});
    this.setState({patientChronicAssignmentList: []});
    this.setState({drugList: []});
    this.setState({drugDataTable: []});
    this.setState({drugListObject: []});
    this.setState({selectedDrugs: []});
    this.setState({drugModalList: []});
    this.setState({chronicAssignmentObject: {drugList: [], chronicDiagnosis: 0, quantity: 0,starterDate: '', frequency: ''}});
    
    this.setState({ dniSelected: false, diseaseSelected: false });
    //this.reset();
  };

  showSuccessModalMessage = message => {
    this.setState({ showModal: true, messageModal: message });
  };

  showDrugInfo = async (prescriptionId, caId) => {
    debugger;
    const drugPromise = await getDrugListByPrescriptionId(prescriptionId);
    this.setState({
      showDrugModal: true,
      drugModalList: drugPromise.data.response
    });
  };
  handleOnClose = () => {
    this.setState({ showDrugModal: false, drugModalList: [] });
  };

  render() {
    const { classes } = this.props;

    if (this.state.loading) {
      return (
        <LoadingScreen
          loading={true}
          bgColor="#f1f1f1"
          spinnerColor="#9ee5f8"
          textColor="#676767"
          text="Cargando"
        />
      );
    }
    if (!this.state.dniSelected) {
      return (
        <DashboardLayout title="Configuración asignaciones crónicas">
          <div className={classes.root}>
            <Grid container spacing={4}>
              {/* <Grid item md={3}></Grid> */}
              <Grid item md={4} sm={6} xs={8}>
                <PatientSearch
                  handlePatientClickSearch={this.handlePatientClickSearch}
                />
              </Grid>
            </Grid>
          </div>
          <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant={this.state.modalVariant}
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
        </DashboardLayout>
      );
    } else if (!this.state.diseaseSelected) {
      return (
        <DashboardLayout title="Configuraciones asignaciones crónicas">
          <div className={classes.root}>
            <Grid container spacing={4}>

              <Grid item md={6} xs={12}>
                <DoctorInfo
                  name={this.state.doctorInfo.name}
                  hospital={this.state.doctorInfo.hospital}
                  license={"M. P. " + this.state.doctorInfo.license}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <PatientInfo
                  name={this.state.patientInfo.name}
                  dni={this.state.patientInfo.dni}
                  age={this.state.patientInfo.age + " años"}
                />
              </Grid>

              {/* <Grid item md={6} xs={12}>
                <DoctorInfo
                  name={this.state.doctorInfo.name}
                  hospital={this.state.doctorInfo.hospital}
                  license={this.state.doctorInfo.license}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <PatientInfo
                  name={this.state.patientInfo.name}
                  dni={this.state.patientInfo.dni}
                  age={this.state.patientInfo.age}
                />
              </Grid> */}
            </Grid>
            <Grid item md={12} xs={12}>
              <br></br>
              <ChronicAsignamentConfiguration
                patientChronicDiagnosisList={
                  this.state.patientChronicDiagnosisList
                }
                chronicAssignmentPatientList={
                  this.state.patientChronicAssignmentList
                }
                handleOnChronicDiagnosisSelect={
                  this.handleOnChronicDiagnosisSelect
                }
                edit={false}
                handleOnShowDrugInfoSelect={this.showDrugInfo}
                drugList={this.state.drugModalList}
                showDrugModal={this.state.showDrugModal}
                handleOnClose={
                this.handleOnClose}
                titleChronicAssignment={this.state.titleChronicAssignment}
                  ></ChronicAsignamentConfiguration>
            </Grid>
          </div>
        </DashboardLayout>
      );
    } else {
      return (
        <DashboardLayout title="Nueva asignación crónica">
          <div className={classes.root}>
            <Grid container spacing={4}>
              <Grid item md={6} xs={12}>
                <DoctorInfo 
                    name={this.state.doctorInfo.name}
                    hospital={this.state.doctorInfo.hospital}
                    license={"M. P. " + this.state.doctorInfo.license}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <PatientInfo
                  name={this.state.patientInfo.name}
                  dni={this.state.patientInfo.dni}
                  age={this.state.patientInfo.age + " años"}
                />
              </Grid>
            </Grid>
            <Grid item md={12} xs={12}>
              <br></br>
              <ChronicPrescription
                //drugs events
                chronicDiseaseSelected={this.state.patientDiseaseSelected}
                drugList={this.state.drugList}
                drugDataTable={this.state.drugDataTable}
                selectedDrugList={this.state.selectedDrugs}
                handleDrugDelete={this.handleDrugDelete}
                handleDrugAdd={this.handleDrugAdd}
                deleteChronicDiagnosisSelected={
                  this.handleOnChronicDiagnosisDelete
                }
                titleChronicAssignment={this.state.titleChronicAssignment}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <br></br>
              <Portlet>
                <PortletHeader>
                  <PortletLabel title="Configuración del reloj" />
                </PortletHeader>
                <PortletContent noPadding>
                  <form autoComplete="off" noValidate>
                    <div className={classes.field}>
                      <AccessAlarm fontSize="large" style={{ marginRight: "12px", marginTop: "15px" }} />
                      <TextField
                        className={classes.textField}
                        label="Frecuencia"
                        margin="dense"
                        onChange={event =>
                          this.setState({ frequency: event.target.value })
                        }
                        required
                        select
                        SelectProps={{ native: true }}
                        value={this.state.frequency}
                        variant="outlined">
                        {frequencies.map(option => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </TextField>

                      <TextField
                        label="Repeticiones"
                        size="small"
                        type="number"
                        margin="dense"
                        variant="outlined"
                        className={classes.textField}
                        value={this.state.quantity}
                        onChange={event =>
                          this.setState({ quantity: event.target.value })
                        }
                      />
                    </div>
                  </form>
                </PortletContent>
              </Portlet>
            </Grid>
            <Grid item md={12} xs={12}>
              <br></br>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button onClick={this.handleOnChronicDiagnosisDelete}>
                  Volver
                </Button>
                <Button
                  onClick={this.saveChronicAssignment}
                  style={{ marginLeft: '30px' }}
                  color="primary"
                  variant="outlined">
                  Guardar
                </Button>
              </div>
            </Grid>
          </div>
          <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>Atención</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant="info"
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
        </DashboardLayout>
      );
    }
  }
}

export default withStyles(styles)(ChronicAsignament);
