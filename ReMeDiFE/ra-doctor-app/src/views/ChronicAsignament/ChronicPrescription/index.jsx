import React from 'react';
import { withStyles } from '@material-ui/core';
import classNames from 'classnames';
import DrugPicker from './DrugPicker';
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent
} from 'components';
import ChronicAsignamentConfiguration from '../ChronicAsignamentConfiguration';
import styles from './styles';

const ChronicPrescription = ({
  chronicDiseaseSelected,
  drugList,
  selectedDrugList,
  drugDataTable,
  handleDrugDelete,
  handleDrugAdd,
  deleteChronicDiagnosisSelected,
  titleChronicAssignment,
  classes,
  className,
  ...rest
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet {...rest} className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Información de la receta crónica" />
      </PortletHeader>
      <PortletContent noPadding>
        <div className={classes.root}>
          <div className={classes.content}>
            <ChronicAsignamentConfiguration
              patientChronicDiagnosisList={chronicDiseaseSelected}
              edit={true}
              deleteChronicDiagnosisSelected={
                deleteChronicDiagnosisSelected
              }
              titleChronicAssignment={titleChronicAssignment}
              ></ChronicAsignamentConfiguration>
            <DrugPicker
              drugList={drugList}
              selectedDrugList={selectedDrugList}
              handleDrugDelete={handleDrugDelete}
              handleDrugAdd={handleDrugAdd}
              drugDataTable={drugDataTable}
            />
          </div>
        </div>
      </PortletContent>
    </Portlet>
  );
};
export default withStyles(styles)(ChronicPrescription);
