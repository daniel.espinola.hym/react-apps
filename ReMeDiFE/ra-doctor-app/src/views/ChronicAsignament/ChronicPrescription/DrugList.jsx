import React from 'react';
import styles from './styles';
import DeleteIcon from '@material-ui/icons/Delete';
import classNames from 'classnames';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Button,
  withStyles
} from '@material-ui/core';

import {
  Portlet,
  PortletContent,
  PortletToolbar,
  PortletHeader,
  PortletLabel
} from 'components';

const DrugList = ({
  className,
  classes,
  drugDataTable,
  handleDrugAdd,
  handleDrugDelete
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Lista de medicamentos" />
        <PortletToolbar>
          <AddCircleOutlineIcon
            style={{ cursor: 'pointer' }}
            onClick={handleDrugAdd}
          />
        </PortletToolbar>
      </PortletHeader>
      <PortletContent noPadding>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Nombre</TableCell>
              <TableCell align="left">Composición</TableCell>
              <TableCell align="left">Concentración</TableCell>
              <TableCell align="left">Forma Farmacéutica</TableCell>
              <TableCell align="left">Días</TableCell>
              <TableCell align="left">Posología</TableCell>
              <TableCell align="left">Acción</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {drugDataTable.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.tableCell} colSpan={7}>
                  No hay medicamentos
                </TableCell>
              </TableRow>
            )}
            {drugDataTable.map(drug => {
              if (!drug.isComercial) {
                //es droga generica
                return (
                  <React.Fragment>
                    <TableRow className={classes.tableRow} hover key={drug.id}>
                      <TableCell className={classes.tableCell}>
                        {drug.name}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <ul>-</ul>
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.strength}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.pharmaceuticalForm}
                      </TableCell>
                      <TableCell className={classes.tableCell} size="small">
                        {drug.days}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.treatment}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <DeleteIcon onClick={() => handleDrugDelete(drug.id)} />
                      </TableCell>
                    </TableRow>
                  </React.Fragment>
                );
              } else {
                //es droga comercial
                debugger;
                return (
                  <React.Fragment>
                    <TableRow className={classes.tableRow} hover key={drug.id}>
                      <TableCell className={classes.tableCell}>
                        {drug.name}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <ul>
                          {drug.composition.map(composition => (
                            <li>{composition.description}</li>
                          ))}
                        </ul>
                      </TableCell>
                      <TableCell className={classes.tableCell}>-</TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.pharmaceuticalForm}
                      </TableCell>
                      <TableCell className={classes.tableCell} size="small">
                        {drug.days}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {drug.treatment}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        <DeleteIcon onClick={() => handleDrugDelete(drug.id)} />
                      </TableCell>
                    </TableRow>
                  </React.Fragment>
                );
              }
            })}
          </TableBody>
        </Table>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(DrugList);
