import React, { useState, useCallback } from 'react';
import { Grid, Button } from '@material-ui/core';
import DrugList from './DrugList';
import DrugModal from './DrugModal';
import { getDrugData } from 'api/drug';

const DrugPicker = React.memo(
  ({
    drugList,
    selectedDrugList,
    drugDataTable,
    handleDrugDelete,
    handleDrugAdd
  }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [prescription, setPrescription] = useState({});
    const [drugData, setDrugData] = useState({});
    const [isComercial, setIsComercial] = useState(false);

    const handleOnClose = useCallback(() => setIsOpen(false), []);

    const openWithoutData = () => {
      setPrescription({});
      setIsOpen(true);
    };

    const openWithData = () => {
      setPrescription({
        drugId: 'paracetamol',
        posologia: 'lablablal',
        cantDias: 5,
        concentracion: '10mg',
        forma: 'oral'
      });
      setIsOpen(true);
    };

    const availableDrugList = drugList.filter(
      //d => !selectedDrugs.includes(d.id) && d.isComercial === this.state.checked
      d => !selectedDrugList.includes(d.idString)
    );

    //parseo necesario para que el componente modal pueda usar la lista de drogas
    const drugsCombo = availableDrugList.map(drug => ({
      label: drug.description,
      value: drug.idString
    }));

    // MANEJO CUANDO SELECCIONO UNA DROGA, LLAMO A LA API PARA TRAERME EXTRA DATA
    const handleOnDrugSelect = useCallback(
      async drugId => {
        console.log(drugList);
        debugger;
        if (drugId.indexOf('C') === 0) {
          setIsComercial(true);
        } else {
          //voy a buscar la data de la droga generica
          const data = await getDrugData(parseFloat(drugId.substr(1)));
          setDrugData(data.data.response);
          setIsComercial(false);
        }
      },
      [drugList]
    );

    // CON ESTO AGREGARIA EL ITEM A LA TABLA (EL STATE DE ESTE COMPONENTE PADRE)
    const handleOnPrescriptionSave = useCallback(
      prescription => {
        console.log(drugData);
        console.log(drugList);
        debugger;
        prescription.name = drugList.find(
          d => d.idString === prescription.id
        ).description;

        if (prescription.id.indexOf('C') === 0) {
          prescription.pharmaceuticalFormDescription = drugList.find(
            d => d.idString === prescription.id
          ).pharmaceuticalForm;

          prescription.composition = drugList
            .find(d => d.idString === prescription.id)
            .drugComponentList.map(component => ({
              description: component.description + ' ' + component.strength
            }));
          prescription.isComercial = true;
        } else {
          prescription.pharmaceuticalFormDescription = drugData.pharmaceuticalFormList.find(
            pm => pm.id === prescription.pharmaceuticalForm
          ).description;
          prescription.strengthDescription = drugData.strengthList.find(
            s => s.id === prescription.strength
          ).description;
          prescription.isComercial = false;
        }
        debugger;
        handleDrugAdd(prescription);
        setIsOpen(false);
      }, // LUEGO LLAMAR A setIsOpen(false) PARA CERRAR MODAL
      [drugData, handleDrugAdd, drugList]
    );

    return (
      <Grid container direction="column">
        <Grid item style={{ marginBottom: 50, marginTop: 50 }}></Grid>
        <Grid item>
          <DrugList
            handleDrugAdd={openWithoutData}
            drugDataTable={drugDataTable}
            handleDrugDelete={handleDrugDelete}
          />
        </Grid>
        <DrugModal
          drugsList={drugsCombo} // LA LISTA DE DROGAS
          editablePrescription={prescription} // SI ESTOY EDITANDO, PASO EL OBJETO QUE EDITO
          isOpen={isOpen} // SI EL MODAL ESTA ABIERTO
          drugData={drugData} // ACA PASO LAS FORMAS Y CONCENTRACIONES
          onDrugSelect={handleOnDrugSelect} // EL EVENTO CUANDO SELECCIONO UNA DROGA, ACA FETCHEARIA LAS FORMAS Y CONCENTRACIONES, COMPUESTOS, ETC.
          onClose={handleOnClose} // CIERRO EL MODAL
          onSaveClick={handleOnPrescriptionSave} // UN EVENTO QUE RECIBE EL OBJETO QUE GUARDO.
          isComercial={isComercial}
        />
      </Grid>
    );
  }
);
export default DrugPicker;
