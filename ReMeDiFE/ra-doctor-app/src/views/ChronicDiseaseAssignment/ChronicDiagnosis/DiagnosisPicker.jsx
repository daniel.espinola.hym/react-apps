import React, { Component, Fragment } from 'react';
import { TextField, Grid, Button } from '@material-ui/core';
import DiagnosisList from './DiagnosisList';

class DiagnosisPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedDiagnosis: 0
    };
  }

  render() {
    debugger;
    const {
      onAddDiagnosisButtonClick,
      onDiagnosisDeletion,
      diagnosisList,
      selectedDiagnosis
    } = this.props;
    debugger;
    const selectedDiagnosisList = diagnosisList.filter(
      d => parseInt(selectedDiagnosis) === d.id
    );
    debugger;
    const availableDiagnosisList = diagnosisList.filter(
      d => parseInt(selectedDiagnosis) !== d.id
    );

    const optionDiagnosisList = [
      { id: 0, description: 'Seleccione...' },
      ...availableDiagnosisList
    ];

    return (
      <Fragment>
        <Grid container direction="column">
          <Grid
            item
            container
            style={{ width: '100%', marginBottom: '30px' }}
            alignItems="center"
            justify="space-between">
            <TextField
              label="Diagnóstico"
              select
              style={{ minWidth: '300px' }}
              onChange={event =>
                this.setState({ selectedDiagnosis: event.target.value })
              }
              SelectProps={{ native: true }}
              variant="outlined">
              {optionDiagnosisList.map(option => (
                <option key={option.id} value={option.id}>
                  {option.description}
                </option>
              ))}
            </TextField>
            <Button
              color="primary"
              variant="outlined"
              onClick={() =>
                onAddDiagnosisButtonClick(this.state.selectedDiagnosis)
              }>
              Agregar
            </Button>
          </Grid>
          <Grid item>
            <DiagnosisList
              diagnosisList={selectedDiagnosisList}
              onDiagnosisDeletion={onDiagnosisDeletion}
            />
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

export default DiagnosisPicker;
