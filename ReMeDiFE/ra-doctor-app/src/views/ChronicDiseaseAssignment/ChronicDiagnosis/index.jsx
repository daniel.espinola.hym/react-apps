import React from 'react';
import { withStyles } from '@material-ui/core';
import classNames from 'classnames';
import DiagnosisPicker from './DiagnosisPicker';
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent
} from 'components';

import styles from './styles';

const ChronicDiagnosis = ({
  diagnosisList,
  selectedDiagnosis,
  onDiagnosisDeletion,
  onAddDiagnosisButtonClick,
  drugList,
  selectedDrugList,
  drugDataTable,
  handleDrugDelete,
  handleDrugAdd,
  classes,
  className,
  ...rest
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet {...rest} className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Elija el diagnóstico crónico que padece el paciente" />
      </PortletHeader>
      <PortletContent noPadding>
        <div className={classes.root}>
          <div className={classes.content}>
            <DiagnosisPicker
              diagnosisList={diagnosisList}
              selectedDiagnosis={selectedDiagnosis}
              onDiagnosisDeletion={onDiagnosisDeletion}
              onAddDiagnosisButtonClick={onAddDiagnosisButtonClick}
            />
          </div>
        </div>
      </PortletContent>
    </Portlet>
  );
};
export default withStyles(styles)(ChronicDiagnosis);
