import React from 'react';
import styles from './styles';
import DeleteIcon from '@material-ui/icons/Delete';
import classNames from 'classnames';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles
} from '@material-ui/core';

import { Portlet, PortletContent } from 'components';

const DiagnosisList = ({
  className,
  classes,
  diagnosisList,
  onDiagnosisDeletion
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet className={rootClassName}>
      <PortletContent noPadding>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Nombre</TableCell>
              <TableCell align="left">Acción</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {diagnosisList.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell className={classes.tableCell} colSpan={5}>
                  No hay diagnósticos
                </TableCell>
              </TableRow>
            )}
            {diagnosisList.map(diagnosis => (
              <TableRow className={classes.tableRow} hover key={diagnosis.id}>
                <TableCell className={classes.tableCell}>
                  {diagnosis.description}
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <DeleteIcon onClick={() => onDiagnosisDeletion()} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(DiagnosisList);
