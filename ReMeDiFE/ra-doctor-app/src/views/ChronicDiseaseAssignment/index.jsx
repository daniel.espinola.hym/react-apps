import React, { Component } from 'react';
import { Grid, Button, withStyles } from '@material-ui/core';

//self components
import DoctorInfo from '../../components/DoctorInfo';
import PatientInfo from '../../components/PatientInfo';
import ChronicDiagnosis from './ChronicDiagnosis';
import PatientSearch from '../../components/PatientSearch';

//external components
import { Dashboard as DashboardLayout } from 'layouts';
import LoadingScreen from 'react-loading-screen';
import Modal from 'react-bootstrap/Modal';
import ButtonBootstrap from 'react-bootstrap/Button';

//APIS
import { getPatientByDni } from 'api/patient';
import { getDoctorInformation } from 'api/doctor';
import { getAllChronicDiagnosis } from 'api/diagnosis';
import { postChronicDiseaseToPatient } from 'api/medicalconsultation';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  content: {
    marginTop: theme.spacing.unit * 2
  },
  progressWrapper: {
    paddingTop: '48px',
    paddingBottom: '24px',
    display: 'flex',
    justifyContent: 'center'
  }
});

class ChronicDiseaseAssignment extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      dniSelected: false,
      doctorInfo: {},
      patientInfo: {
        name: '',
        dni: '',
        age: '',
        id: 0
      },
      diagnosisList: [],
      selectedDiagnosis: 0,
      showModal: false,
      messageModal: '',
      modalTitle: '',
      modalVariant: ''
    };
  }

  async componentDidMount() {
    //Voy a buscar la informacion del doctor que esta logeado
    const doctorInfo = await getDoctorInformation();
    debugger;
    //Voy a buscar todos los diagnosticos
    const diagnosisPromise = await getAllChronicDiagnosis();
    //Seteo la info de los diagnosis
    const diagnosisList = diagnosisPromise.data.response;

    this.setState({ doctorInfo, diagnosisList, loading: false });
  }

  //diagnosis components functions
  handleOnAddDiagnosisButtonClick = diagnosisId => {
    this.setState({ selectedDiagnosis: diagnosisId });
  };

  handleOnDiagnosisDeletion = () => this.setState({ selectedDiagnosis: 0 });
  //fin diagnosis

  saveChronicDisease = () => {
    let data = {};
    data.doctorId = this.state.doctorInfo.id;
    data.hospitalId = this.state.doctorInfo.hospitalId;
    data.patientId = this.state.patientInfo.id;
    data.chronicDiagnosisId = parseInt(this.state.selectedDiagnosis);
    console.log(data);
    //asd
    postChronicDiseaseToPatient(data)
      .then(r => {
        console.log(r);
        // if(r.response != null){
        this.showSuccessModalMessage('La operación terminó con éxito');
        // }else{
        //   this.showSuccessModalMessage('El paciente ya tiene esa enfermedad crónica asignada');
        // }
      })
      .catch(error => {
        console.log('todo error');
        console.log(error.response);
      });
  };

  handlePatientClickSearch = async event => {
    try {
      const r = await getPatientByDni(parseInt(event));
      console.log(r);
      if (r.data.error != null) {
        // this.setState({ loading: false});
        this.setState({ modalVariant: "danger" });
        this.setState({ showModal: true });
        this.setState({ messageModal: r.data.error });
        this.setState({ modalTitle: "Error" });
      } else {
        this.setState({ modalVariant: "info" });
        let patient = {};
        patient.dni = r.data.response.dni;
        patient.name = r.data.response.fullName;
        patient.age = r.data.response.age;
        patient.id = r.data.response.id;
        this.setState({ dniSelected: true, patientInfo: patient });
        this.setState({ modalTitle: "Atención" });
        
      }
    } catch (ex) {
      debugger;
      this.showError();
    }
  };

  reset = () => {
    this.setState({diagnosisList: []});
    this.setState({ dniSelected: false, selectedDiagnosis: 0 });
  };

  handleModalClose = () => {
    this.setState({ showModal: false, messageModal: '' });
    this.reset();
  };

  showSuccessModalMessage = message => {
    this.setState({ showModal: true, messageModal: message });
  };

  render() {
    const { loading } = this.state;
    const { classes } = this.props;
    if (loading) {
      return (
        <LoadingScreen
          loading={true}
          bgColor="#f1f1f1"
          spinnerColor="#9ee5f8"
          textColor="#676767"
          text="Cargando"
        />
      );
    }
    if (!this.state.dniSelected) {
      return (
        <DashboardLayout title="Alta de paciente crónico">
          <div className={classes.root}>
            <Grid container spacing={4}>
            <Grid item md={4} sm={6} xs={8}>
                <PatientSearch
                  handlePatientClickSearch={this.handlePatientClickSearch}
                />
              </Grid>
            </Grid>
          </div>
          <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant={this.state.modalVariant}
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
        </DashboardLayout>
      );
    }
    return (
      <DashboardLayout title="Alta de paciente crónico">
        <div className={classes.root}>
          <Grid container spacing={4}>
            <Grid item md={6} xs={12}>
              <DoctorInfo
                name={this.state.doctorInfo.name}
                hospital={this.state.doctorInfo.hospital}
                license={"M. P. " + this.state.doctorInfo.license}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <PatientInfo
                name={this.state.patientInfo.name}
                dni={this.state.patientInfo.dni}
                age={this.state.patientInfo.age + " años"}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <ChronicDiagnosis
                //diagnosis events
                diagnosisList={this.state.diagnosisList}
                selectedDiagnosis={this.state.selectedDiagnosis}
                onDiagnosisDeletion={this.handleOnDiagnosisDeletion}
                onAddDiagnosisButtonClick={this.handleOnAddDiagnosisButtonClick}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button onClick={this.reset}>Volver</Button>
                <Button
                  style={{ marginLeft: '30px' }}
                  color="primary"
                  variant="outlined"
                  onClick={this.saveChronicDisease}>
                  Guardar
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
        <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant="info"
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
      </DashboardLayout>
    );
  }
}

export default withStyles(styles)(ChronicDiseaseAssignment);
