import React, { Component } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Grid,
  withStyles
} from '@material-ui/core';
import { Edit, Search } from '@material-ui/icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileSignature } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

//self components
import DrugTableModal from './DrugTableModal';

//external components
import LoadingScreen from 'react-loading-screen';
import { Dashboard as DashboardLayout } from 'layouts';
import {
  Portlet,
  PortletContent,
  PortletHeader,
  PortletLabel
} from 'components';

//APIS
import {
  signPrescription,
  getPrescriptionPendingByDoctorId,
  getPrescriptionPreview
} from 'api/medicalconsultation';
import { generatePrescriptionWithSignature } from 'api/signature';
import { getDrugListByPrescriptionId } from 'api/drug';

import Modal from 'react-bootstrap/Modal';
import ButtonBootstrap from 'react-bootstrap/Button';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  content: {
    marginTop: theme.spacing.unit * 2
  },
  progressWrapper: {
    paddingTop: '48px',
    paddingBottom: '24px',
    display: 'flex',
    justifyContent: 'center'
  },
  tableRow: {
    height: '64px'
  },
  tableCell: {
    whiteSpace: 'normal'
  },
  tableCellInner: {
    display: 'flex',
    alignItems: 'center'
  }
});

class PrescriptionPending extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      prescriptionPendingList: [],
      showDrugModal: false,
      drugList: []
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const prescriptionPendingPromise = await getPrescriptionPendingByDoctorId();
    this.setState({
      loading: false,
      prescriptionPendingList: prescriptionPendingPromise.data.response
    });
  }

  handleModalClose = () => {
    this.setState({ showModal: false, messageModal: '' });
    //this.setState({ dniSelected: false, diseaseSelected: false });
    //this.reset();
  };

  showSuccessModalMessage = async (message) => {
    this.setState({ showModal: true, messageModal: message });
    this.setState({ loading: true });
    const prescriptionPendingPromise = await getPrescriptionPendingByDoctorId();
    this.setState({
      loading: false,
      prescriptionPendingList: prescriptionPendingPromise.data.response
    });
  };

  handleSignPrescrition(mcId, patientId, prescriptionId) {
    var pwd = prompt('Ingrese la contraseña del token', '');
    if (pwd != null || pwd !== '') {
      try {
        this.requestAndSaveSign(mcId, patientId, prescriptionId, pwd);
      } catch (ex) {
        alert("Error interno")
        console.log(ex);
      }
    } else {
      alert("Debe ingresar la contraseña");
    }
  }

  requestAndSaveSign = async (medicalConsultationId, patientId, prescriptionId, pwd) => {
    const prescriptionResponse = await getPrescriptionPreview(patientId, medicalConsultationId);
    prescriptionResponse.data.response.password = pwd;
    const localTaskResponse = await generatePrescriptionWithSignature(prescriptionResponse.data.response);
    let fileObject = {
      file: localTaskResponse.data
    };
    const backResponse = await signPrescription(prescriptionId, fileObject);
    this.showSuccessModalMessage('La operación terminó con éxito');
  };

  showDrugInfo = async prescriptionId => {
    const drugPromise = await getDrugListByPrescriptionId(prescriptionId);
    this.setState({ showDrugModal: true, drugList: drugPromise.data.response });
  };
  handleOnClose = () => {
    this.setState({ showDrugModal: false, drugList: [] });
  };

  render() {
    const { loading, prescriptionPendingList } = this.state;
    debugger;
    const { classes } = this.props;
    if (loading) {
      return (
        <LoadingScreen
          loading={true}
          bgColor="#f1f1f1"
          spinnerColor="#9ee5f8"
          textColor="#676767"
          text="Cargando"
        />
      );
    }
    return (
      <DashboardLayout title="Recetas pendientes de firma">
        <div>
          <Grid container spacing={4}>
            <Grid item md={12} xs={12}>
              <Portlet>
                <PortletHeader>
                  <PortletLabel title="Seleccione la receta que desea firmar" />
                  {/* "Seleccione para firmar" */}
                </PortletHeader>
                <PortletContent noPadding>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="left">Identificador</TableCell>
                        <TableCell align="left">Fecha</TableCell>
                        <TableCell align="left">Paciente</TableCell>
                        <TableCell align="left">Hospital</TableCell>
                        <TableCell align="left">Diagnóstico</TableCell>
                        <TableCell align="left">Acción</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {prescriptionPendingList.length === 0 && (
                        <TableRow className={classes.tableRow}>
                          <TableCell className={classes.tableCell} colSpan={6}>
                            No hay pendientes
                          </TableCell>
                        </TableRow>
                      )}
                      {prescriptionPendingList.map(p => (
                        <TableRow className={classes.tableRow} hover key={p.id}>
                          <TableCell className={classes.tableCell}>
                            {p.id}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {p.date}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {p.patientName}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {p.hospitalName}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            <ul>
                              {p.diagnosisList.map(diagnosis => (
                                <li>{diagnosis.name}</li>
                              ))}
                            </ul>
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {/* <Search
                              style={{ cursor: 'pointer' }}
                              onClick={() => this.showDrugInfo(p.id)}
                            /> */}
                            <FontAwesomeIcon icon={faFileSignature} 
                            title="Firmar"
                            size="3x"
                            style={{ cursor: 'pointer', paddingRight: "6px"}}
                            onClick={() =>
                              this.handleSignPrescrition(
                                p.medicalConsultationId,
                                p.patientId,
                                p.id
                              )
                            }
                            />
                            <FontAwesomeIcon icon={faSearch} 
                            title="Ver medicamentos"
                            size="2x"
                            style={{ cursor: 'pointer', paddingBottom:"3px" }}
                            onClick={() => this.showDrugInfo(p.id)}
                            />

                            {/* <Edit
                              style={{ cursor: 'pointer' , color: '#008388'}}
                              onClick={() =>
                                this.handleSignPrescrition(
                                  p.medicalConsultationId,
                                  p.patientId,
                                  p.id
                                )
                              }
                            /> */}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </PortletContent>
              </Portlet>
            </Grid>

            <DrugTableModal
              drugList={this.state.drugList} // LA LISTA DE DROGAS
              isOpen={this.state.showDrugModal} // SI EL MODAL ESTA ABIERTO
              onClose={this.handleOnClose} // CIERRO EL MODAL
            />
          </Grid>
        </div>
        <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>Atención</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant="info"
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
      </DashboardLayout>
    );
  }
}

export default withStyles(styles)(PrescriptionPending);
