import React from 'react';
import classNames from 'classnames';
import styles from './styles';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles
} from '@material-ui/core';

import { Portlet, PortletContent } from 'components';

const PatientList = ({ className, classes, patientList }) => {
  console.log(classes.root);
  const rootClassName = classNames(classes.root, className);

  return (
    <Portlet className={rootClassName}>
      <PortletContent noPadding>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="left">Nombre</TableCell>
              <TableCell align="left">DNI</TableCell>
              <TableCell align="left">Fecha Atención</TableCell>
              <TableCell align="left">Diagnóstico</TableCell>
              <TableCell align="left">Asignación Crónica</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {patientList.length === 0 && (
              <TableRow className={classes.tableRow}>
                <TableCell colSpan={5}>No hay pacientes</TableCell>
              </TableRow>
            )}
            {patientList.map(patient => {
              return (
                <TableRow
                  className={classes.tableRow}
                  hover
                  key={patient.prescriptionId}>
                  <TableCell>{patient.patientName}</TableCell>
                  <TableCell>{patient.patientDni}</TableCell>
                  <TableCell>{patient.consultationDate}</TableCell>
                  <TableCell>{patient.diagnosis}</TableCell>
                  <TableCell>{patient.hasChronicAssignment}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(PatientList);
