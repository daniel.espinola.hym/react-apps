import React, { Component } from 'react';

// Shared layouts
import { Dashboard as DashboardLayout } from 'layouts';

import PatientTable from './PatientList';
import { withStyles } from '@material-ui/core';

import { getOwnChronicPatientList } from 'api/doctor';

// Component styles
import styles from './styles';

class PatientList extends Component {
  state = {
    isLoading: false,
    patientList: [],
    error: null
  };

  async componentDidMount() {
    this.signal = true;
    const patientListResponse = await getOwnChronicPatientList();
    this.setState({ patientList: patientListResponse.data.response });
  }

  renderPatientList() {
    const { patientList } = this.state;
    return <PatientTable patientList={patientList} />;
  }

  render() {
    return (
      <DashboardLayout title="Consultas de pacientes crónicos">
        {this.renderPatientList()}
      </DashboardLayout>
    );
  }
}

export default withStyles(styles)(PatientList);
