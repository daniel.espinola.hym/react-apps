import React, { Component } from 'react';
import { Grid, Button, withStyles } from '@material-ui/core';

//self components
import DoctorInfo from 'components/DoctorInfo';
import PatientInfo from 'components/PatientInfo';
import Prescription from './Prescription';
import PatientSearch from 'components/PatientSearch';

//external components
import { Dashboard as DashboardLayout } from 'layouts';
import LoadingScreen from 'react-loading-screen';

//APIS
import { getPatientByDni } from 'api/patient';
import { getDoctorInformation } from 'api/doctor';
import { getAllDiagnosis } from 'api/diagnosis';
import {
  postPrescription,
  signPrescription,
  getPrescriptionSigned,
  getPrescriptionPreview
} from 'api/medicalconsultation';
import { getAllDrugsBack } from 'api/drug';
import { generatePrescriptionWithSignature } from 'api/signature';

import Modal from 'react-bootstrap/Modal';
import ButtonBootstrap from 'react-bootstrap/Button';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  content: {
    marginTop: theme.spacing.unit * 2
  },
  progressWrapper: {
    paddingTop: '48px',
    paddingBottom: '24px',
    display: 'flex',
    justifyContent: 'center'
  }
});

class MedicalConsultation extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      dniSelected: false,
      doctorInfo: {
        name: '',
        license: '',
        hospital: ''
      },
      patientInfo: {
        name: '',
        dni: '',
        age: '',
        id: 0
      },
      diagnosisList: [],
      drugList: [],
      drugGenericList: [],
      drugComercialList: [],
      selectedDrugs: [],
      drugListObject: [],
      drugDataTable: [],
      selectedDiagnosis: [],
      showModal: false,
      messageModal: '',
      modalTitle: '',
      modalVariant: ''
    };
  }

  async componentDidMount() {
    //Voy a buscar la informacion del doctor que esta logeado
    const doctorInfo = await getDoctorInformation();
    //Voy a buscar todos los diagnosticos
    const diagnosisPromise = await getAllDiagnosis();
    //Seteo la info de los diagnosis
    const diagnosisList = diagnosisPromise.data.response;
    const drugPromise = await getAllDrugsBack();
    const drugGenericList = drugPromise.data.response.drugGenericList;
    const drugComercialList = drugPromise.data.response.drugComercialList;
    const drugList = [];
    drugGenericList.forEach(drugGeneric => {
      drugList.push(drugGeneric);
    });
    drugComercialList.forEach(drugComercial => {
      drugList.push(drugComercial);
    });
    this.setState({
      doctorInfo,
      diagnosisList,
      drugGenericList,
      drugComercialList,
      drugList,
      loading: false
    });
  }

  //diagnosis components functions
  handleOnAddDiagnosisButtonClick = diagnosisId => {
    const { selectedDiagnosis } = this.state;

    if (selectedDiagnosis.includes(diagnosisId)) {
      return;
    }

    this.setState({
      selectedDiagnosis: [
        ...selectedDiagnosis,
        Number.parseInt(diagnosisId, 10)
      ]
    });
  };

  handleOnDiagnosisDeletion = diagnosisId =>
    this.setState({
      selectedDiagnosis: this.state.selectedDiagnosis.filter(
        d => d !== diagnosisId
      )
    });
  //fin diagnosis

  //drugs components functions
  handleDrugAdd = async selectedDrugObject => {
    const { selectedDrugs, drugListObject, drugDataTable } = this.state;

    if (selectedDrugs.includes(selectedDrugObject.id)) {
      return;
    } else {
      selectedDrugs.push(selectedDrugObject.id);
    }

    const drugObject = {
      id: selectedDrugObject.id,
      name: selectedDrugObject.name,
      days: selectedDrugObject.days,
      strength: selectedDrugObject.strength,
      pharmaceuticalForm: selectedDrugObject.pharmaceuticalForm,
      treatment: selectedDrugObject.treatment
    };

    if (selectedDrugObject.isComercial) {
      const drugTableObject = {
        id: selectedDrugObject.id,
        name: selectedDrugObject.name,
        days: selectedDrugObject.days,
        composition: selectedDrugObject.composition,
        pharmaceuticalForm: selectedDrugObject.pharmaceuticalFormDescription,
        treatment: selectedDrugObject.treatment,
        isComercial: true
      };

      this.setState({
        drugListObject: [...drugListObject, drugObject],
        drugDataTable: [...drugDataTable, drugTableObject]
      });
    } else {
      const drugTableObject = {
        id: selectedDrugObject.id,
        name: selectedDrugObject.name,
        days: selectedDrugObject.days,
        strength: selectedDrugObject.strengthDescription,
        pharmaceuticalForm: selectedDrugObject.pharmaceuticalFormDescription,
        treatment: selectedDrugObject.treatment,
        isComercial: false
      };

      this.setState({
        drugListObject: [...drugListObject, drugObject],
        drugDataTable: [...drugDataTable, drugTableObject]
      });
    }
  };

  handleDrugDelete = drugId => {
    this.setState({
      selectedDrugs: this.state.selectedDrugs.filter(d => d !== drugId)
    });
    this.setState({
      drugListObject: this.state.drugListObject.filter(d => d.id !== drugId)
    });
    this.setState({
      drugDataTable: this.state.drugDataTable.filter(d => d.id !== drugId)
    });
  };

  //fin drugs

  savePrescription = () => {
    let data = {};
    data.doctorId = this.state.doctorInfo.id;
    data.hospitalId = this.state.doctorInfo.hospitalId;
    data.patientId = this.state.patientInfo.id;
    data.prescription = {};
    data.prescription.diagnosisList = this.state.selectedDiagnosis;
    data.prescription.drugList = this.state.drugListObject;
    postPrescription(data)
      .then(r => {
        var pwd = prompt('Ingrese la contraseña del token', '');
        while (pwd == null || pwd == '') {
          pwd = prompt('Ingrese la contraseña del token', '');
        }

        this.signPrescriptionFunction(r.data.response.id, data.patientId, pwd);
      })
      .catch(error => {
        alert("Error interno");
        console.log(error.response);
      });
  };

  onTreatmentBoxChange = event => {
    this.setState({ treatment: event.target.value });
  };

  handlePatientClickSearch = async event => {
    try {
      const r = await getPatientByDni(parseInt(event));

      if (r.data.error != null) {
        // this.setState({ loading: false});
        this.setState({ modalVariant: "danger" });
        this.setState({ showModal: true });
        this.setState({ messageModal: r.data.error });
        this.setState({ modalTitle: "Error" });
      } else {
        this.setState({ modalVariant: "info" });
        let patient = {};
        patient.dni = r.data.response.dni;
        patient.name = r.data.response.fullName;
        patient.age = r.data.response.age;
        patient.id = r.data.response.id;
        this.setState({ patientInfo: patient, dniSelected: true });
      }
    } catch (ex) {
      alert("Error interno");
      console.log(ex);
    }
  };

  handleModalClose = () => {
    this.setState({ showModal: false, messageModal: '' });
    //this.setState({ dniSelected: false, diseaseSelected: false });
    this.reset();
  };

  showSuccessModalMessage = message => {
    this.setState({ showModal: true, messageModal: message });
  };

  reset = () => {
    this.setState({diagnosisList: []});
    this.setState({drugList: []});
    this.setState({drugGenericList: []});
    this.setState({drugComercialList: []});
    this.setState({selectedDrugs: []});
    this.setState({drugListObject: []});
    this.setState({drugDataTable: []});
    this.setState({selectedDiagnosis: []});
    this.setState({ dniSelected: false });
  };

  signPrescriptionFunction = async (medicalConsultationId, patientId, pwd) => {
    //get prescription model
    const prescriptionResponse = await getPrescriptionPreview(patientId, medicalConsultationId);
    prescriptionResponse.data.response.password = pwd;
    const localTaskResponse = await generatePrescriptionWithSignature(prescriptionResponse.data.response);
    let fileObject = {
      file: localTaskResponse.data
    };
    const backResponse = signPrescription(prescriptionResponse.data.response.id, fileObject);
    this.showSuccessModalMessage('La operación terminó con éxito');
  };

  getPrescriptionSignedBy = async prescriptionId => {
    const response = await getPrescriptionSigned(prescriptionId);
  };

  render() {
    const { loading } = this.state;
    const { classes } = this.props;
    if (loading) {
      return (
        <LoadingScreen
          loading={true}
          bgColor="#f1f1f1"
          spinnerColor="#9ee5f8"
          textColor="#676767"
          text="Cargando"
        />
      );
    }
    if (!this.state.dniSelected) {
      return (
        <DashboardLayout title="Nueva Receta">
          <div className={classes.root}>
            <Grid container spacing={4}>
              <Grid item md={4} sm={6} xs={8}>
                <PatientSearch
                  handlePatientClickSearch={this.handlePatientClickSearch}
                />
              </Grid>
            </Grid>
          </div>
          <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant={this.state.modalVariant}
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
        </DashboardLayout>
      );
    }
    return (
      <DashboardLayout title="Nueva Receta">
        <div className={classes.root}>
          <Grid container spacing={4}>
            <Grid item md={6} xs={12}>
              <DoctorInfo 
                name={this.state.doctorInfo.name}
                hospital={this.state.doctorInfo.hospital}
                license={"M. P. " + this.state.doctorInfo.license}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <PatientInfo 
                name={this.state.patientInfo.name}
                dni={this.state.patientInfo.dni}
                age={this.state.patientInfo.age + " años"}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <Prescription
                //diagnosis events
                diagnosisList={this.state.diagnosisList}
                selectedDiagnosis={this.state.selectedDiagnosis}
                onDiagnosisDeletion={this.handleOnDiagnosisDeletion}
                onAddDiagnosisButtonClick={this.handleOnAddDiagnosisButtonClick}
                //drugs events
                drugList={this.state.drugList}
                drugDataTable={this.state.drugDataTable}
                selectedDrugList={this.state.selectedDrugs}
                handleDrugDelete={this.handleDrugDelete}
                handleDrugAdd={this.handleDrugAdd}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button>Volver</Button>
                <Button
                  style={{ marginLeft: '30px' }}
                  color="primary"
                  variant="outlined"
                  onClick={this.savePrescription}>
                  Guardar
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
        <div>
          <Modal
            centered
            show={this.state.showModal}
            onHide={this.state.handleModalClose}>
            <Modal.Header closeButton>
              <Modal.Title>Atención</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.state.messageModal}</Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant="info"
                onClick={this.handleModalClose}>
                Cerrar
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </div>
      </DashboardLayout>
    );
  }
}

export default withStyles(styles)(MedicalConsultation);
