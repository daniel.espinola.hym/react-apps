import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Autocomplete from '../../../components/Utils/Autocomplete';

const createStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  formGroup: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  formControl: {
    margin: theme.spacing(1),
    width: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

const DrugModal = React.memo(
  ({
    onSaveClick,
    onClose,
    onDrugSelect,
    drugsList,
    editablePrescription,
    drugData = {},
    isComercial,
    isOpen
  }) => {
    const classes = createStyles();
    const [prescription, setPrescription] = useState({});
    const { strengthList, pharmaceuticalFormList } = drugData;
    // USO UN HOOK PARA POPULAR LOS DATOS EN EL MODAL SI ESTOY EDITANDO
    useEffect(() => {
      const {
        id,
        days,
        pharmaceuticalForm,
        strength,
        treatment
      } = editablePrescription;
      setPrescription({
        id,
        days,
        pharmaceuticalForm,
        strength,
        treatment
      });
    }, [editablePrescription]);

    // CUANDO UNA DROGA ES SELECCIONADA, INVOCO A ONDRUGSELECT DE MI PADRE PARA TRAERME LOS DATOS DE LA DROGA.
    useEffect(() => {
      if (prescription.id) {
        onDrugSelect(prescription.id);
      }
    }, [onDrugSelect, prescription.id]);

    // SETEO EL DRUG ID EN MI LOCAL STATE
    const handleOnDrugSelect = useCallback(
      input => {
        setPrescription({ ...prescription, id: input.value });
      },
      [prescription]
    );

    // SETEO LOS VALORES EN MI LOCAL STATE DE FORMA, CONCENTRACION, DIAS Y POSOLOGIA
    const handleInputChange = useCallback(
      event =>
        setPrescription({
          ...prescription,
          [event.target.name]: event.target.value
        }),
      [prescription]
    );

    // INVOCO A LA FUNCION ONSAVE DE MI PADRE, Y PASO LO QUE TENGO GUARDADO EN MI ESTADO LOCAL
    const handleOnSave = useCallback(() => onSaveClick(prescription), [
      onSaveClick,
      prescription
    ]);

    return (
      <Dialog
        open={isOpen}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
        maxWidth="md"
        fullWidth>
        {/* <DialogTitle id="form-dialog-title">
          {!!prescription && prescription.id ? 'Edit ' : 'Add '} drug
        </DialogTitle> */}
        <DialogContent>
          <form className={classes.root} autoComplete="off">
            <Autocomplete
              options={drugsList}
              label="Medicamento"
              name="drug-select"
              placeholder="Seleccione..."
              initialValue={prescription.id}
              onSelect={handleOnDrugSelect}
            />
            <div className={classes.formGroup}>
              {!isComercial && (
                <React.Fragment>
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="ceoncentracion">
                      Concentración
                    </InputLabel>
                    <Select
                      value={prescription.strength}
                      onChange={handleInputChange}
                      name="strength"
                      id="strength">
                      {(strengthList || []).map(({ id, description }) => (
                        <MenuItem value={id}>{description}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="pharmaceuticalForm">
                      Forma Farmacéutica
                    </InputLabel>
                    <Select
                      value={prescription.pharmaceuticalForm}
                      onChange={handleInputChange}
                      name="pharmaceuticalForm"
                      id="pharmaceuticalForm">
                      {(pharmaceuticalFormList || []).map(
                        ({ id, description }) => (
                          <MenuItem value={id}>{description}</MenuItem>
                        )
                      )}
                    </Select>
                  </FormControl>
                </React.Fragment>
              )}

              <FormControl className={classes.formControl}>
                <TextField
                  label="Cant. Días"
                  type="number"
                  name="days"
                  id="days"
                  value={prescription.days}
                  onChange={handleInputChange}
                />
              </FormControl>
            </div>
            <div className={classes.formGroup}>
              <TextField
                label="Posología"
                multiline
                rows="4"
                name="treatment"
                value={prescription.treatment}
                onChange={handleInputChange}
                className={classes.formControl}
                margin="normal"
              />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleOnSave} color="primary">
            Agregar
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
);

export default DrugModal;
