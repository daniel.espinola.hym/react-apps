import React from 'react';
import { withStyles } from '@material-ui/core';
import classNames from 'classnames';
import DiagnosisPicker from './DiagnosisPicker';
import DrugPicker from './DrugPicker';
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent
} from 'components';

import styles from './styles';

const Prescription = ({
  diagnosisList,
  selectedDiagnosis,
  onDiagnosisDeletion,
  onAddDiagnosisButtonClick,
  drugList,
  selectedDrugList,
  drugDataTable,
  handleDrugDelete,
  handleDrugAdd,
  classes,
  className,
  ...rest
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet {...rest} className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Información de la receta" />
      </PortletHeader>
      <PortletContent noPadding>
        <div className={classes.root}>
          <div className={classes.content}>
            <DiagnosisPicker
              diagnosisList={diagnosisList}
              selectedDiagnosis={selectedDiagnosis}
              onDiagnosisDeletion={onDiagnosisDeletion}
              onAddDiagnosisButtonClick={onAddDiagnosisButtonClick}
            />
            <DrugPicker
              drugList={drugList}
              selectedDrugList={selectedDrugList}
              handleDrugDelete={handleDrugDelete}
              handleDrugAdd={handleDrugAdd}
              drugDataTable={drugDataTable}
            />
          </div>
        </div>
      </PortletContent>
    </Portlet>
  );
};
export default withStyles(styles)(Prescription);
