import React, { Component } from 'react';
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment
} from 'semantic-ui-react';
import AuthenticationService from 'common/services/AuthenticationService';
import { Redirect } from 'react-router';
import Modal from 'react-bootstrap/Modal';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import ButtonBootstrap from 'react-bootstrap/Button';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      hasLoginFailed: false,
      showSuccessMessage: false,
      redirect: false,
      errorMsg: '',
      showHospitalModal: false,
      hospitalList: [],
      selectedHospital: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.loginClicked = this.loginClicked.bind(this);
    this.handleListItemClick = this.handleListItemClick.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.selectHospitalAndAuthenticate = this.selectHospitalAndAuthenticate.bind(
      this
    );
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleListItemClick(idHospital) {
    console.log(idHospital);
    this.setState({ selectedHospital: idHospital });
  }

  handleCloseModal() {
    this.setState({ showHospitalModal: false });
  }

  loginClicked() {
    AuthenticationService.executeJwtPreAuthenticationService(
      this.state.username,
      this.state.password
    )
      .then(response => {
        console.log(response);
        this.setState({ hospitalList: response.data });
        this.setState({ showHospitalModal: true });
      })
      .catch(exception => {
        debugger;
        console.log(exception);
        if (exception.response) {
          this.setState({ errorMsg: exception.response.data });
          this.setState({ showSuccessMessage: false });
          this.setState({ hasLoginFailed: true });
        }
      });
  }

  selectHospitalAndAuthenticate() {
    AuthenticationService.executeJwtAuthenticationService(
      this.state.username,
      this.state.password,
      this.state.selectedHospital
    )
      .then(response => {
        AuthenticationService.registerSuccessfulLoginForJwt(
          this.state.username,
          response.data.token
        );
        console.log(AuthenticationService.getIdHospital());
        this.setState({ showHospitalModal: false });
        this.setState({ redirect: true });
      })
      .catch(exception => {
        debugger;
        console.log(exception);
        this.setState({ errorMsg: exception.response.data });
        this.setState({ showSuccessMessage: false });
        this.setState({ hasLoginFailed: true });
      });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/medicalConsultation" />;
    } else {
      return (
        <Grid
          textAlign="center"
          style={{ height: '100vh' }}
          verticalAlign="middle">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" textAlign="center">
              <Image
                style={{ width: '200px', height: 'auto' }}
                src="/images/logos/ReMeDi_white_logo.png"
              />
            </Header>
            <Form size="large">
              <Segment stacked>
                <Form.Input
                  fluid
                  icon="user"
                  name="username"
                  id="username"
                  iconPosition="left"
                  placeholder="DNI"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  name="password"
                  id="password"
                  placeholder="Contraseña"
                  type="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />

                <Button
                  onClick={this.loginClicked}
                  color="teal"
                  fluid
                  size="large">
                  Ingresar
                </Button>
              </Segment>
            </Form>
            {this.state.hasLoginFailed && (
              <Message negative content={this.state.errorMsg}></Message>
            )}
          </Grid.Column>
          <Modal
            centered
            show={this.state.showHospitalModal}
            onHide={this.handleCloseModal}>
            <Modal.Header closeButton>
              <Modal.Title>
                Seleccione el hospital con el cual quiere ingresar
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.state.modalMsg}
              <List component="nav" aria-label="secondary mailbox folder">
                {this.state.hospitalList.map(hospital => (
                  <ListItem
                    button
                    selected={
                      this.state.selectedHospital === hospital.idHospital
                    }
                    onClick={() => {
                      this.handleListItemClick(hospital.idHospital);
                    }}>
                    <ListItemText primary={hospital.hospitalName} />
                  </ListItem>
                ))}
              </List>
            </Modal.Body>
            <Modal.Footer>
              <ButtonBootstrap
                variant="success"
                onClick={this.selectHospitalAndAuthenticate}>
                OK
              </ButtonBootstrap>
            </Modal.Footer>
          </Modal>
        </Grid>
      );
    }
  }
}
