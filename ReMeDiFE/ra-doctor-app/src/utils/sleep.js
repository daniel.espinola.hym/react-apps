export default (time, data) =>
  new Promise(resolve => setTimeout(() => resolve(data), time));
