import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const stylesheet = {
  loaderContainer: {
    display: "flex",
    height: "100%",
    background: "rgba(0, 0, 0, 0.4)",
    alignItems: "center",
    justifyContent: "center"
  }
};

const Loader = ({ show }) => {
  if (!show) {
    return null;
  }

  return (
    <div style={stylesheet.loaderContainer}>
      <CircularProgress />
    </div>
  );
};

export default Loader;
