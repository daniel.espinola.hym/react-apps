import React, { useState } from 'react';
import { Grid, withStyles, TextField, Button } from '@material-ui/core';
import classNames from 'classnames';
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from 'components';

const styles = theme => ({
  root: {},
  form: {},
  textField: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  },
  portletFooter: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
  // root: {},
  // form: {
  //   display: 'flex',
  //   flexWrap: 'wrap',
  //   alignItems: 'center'
  // },
  // group: {
  //   //flexGrow: 1,
  //   padding: theme.spacing.unit * 2
  // },
  // groupLabel: {
  //   paddingLeft: theme.spacing.unit * 2
  // },
  // field: {
  //   marginBottom: theme.spacing.unit * 2,
  //   marginTop: theme.spacing.unit * 2,
  //   display: 'flex',
  //   alignItems: 'center'
  // },
  // textField: {
  //   width: '430px',
  //   maxWidth: '100%',
  //   marginRight: theme.spacing.unit * 5
  // }
});

const PatientSearch = ({
  handlePatientClickSearch,
  classes,
  className,
  ...rest
}) => {
  const [dni, setDni] = useState('');
  const rootClassName = classNames(classes.root, className);
  const functionClick = () => {
    handlePatientClickSearch(dni);
  };
  const handleChange = e => {
    setDni(e.target.value);
  };
  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      functionClick();
      e.preventDefault();
    }
  }
  return (
    <Portlet {...rest} className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Ingrese el DNI del paciente" />
      </PortletHeader>
      <PortletContent>
         <form className={classes.form}>
          {/* <div className={classes.group}>
            <div className={classes.field}>
              <div> */}
                <TextField
                  className={classes.textField}
                  label="DNI"
                  margin="dense"
                  // inputProps={{ maxLength: 15} }
                  required
                  variant="outlined"
                  onChange={handleChange}
                  onKeyDown={handleKeyDown}
                  // onKeyPress={handleKeyDown}
                />
              {/* </div>
            </div>
            <div className={classes.field}>
              <div> */}
                {/* <Button
                  // style={{ marginLeft: '173px' }}
                  color="primary"
                  variant="outlined"
                  onClick={functionClick}>
                  Buscar
                </Button> */}
              {/* </div>
            </div>
          </div> */}
        </form> 


       </PortletContent>
       <PortletFooter className={classes.portletFooter}>
          <Button 
            color="primary"
            variant="outlined"
            onClick={functionClick}
            size="medium"
            fullWidth={true}>
            Buscar
          </Button>
        </PortletFooter>
       </Portlet>

//       <Portlet {...rest} className={rootClassName}>
// {/*       <Portlet
//         {...rest}
//         className={rootClassName}
//       > */}
//         <PortletHeader>
//           <PortletLabel
//             // subtitle="Update password"
//             title="Ingresar Receta"
//           />
//         </PortletHeader>
//         <PortletContent>
//           <form className={classes.form}>
//                  <TextField
//                   className={classes.textField}
//                   label="DNI"
//                   //margin="dense"
//                   //inputProps={{ maxLength: 10 }}
//                   required
//                   variant="outlined"
//                   onChange={handleChange}
//                   onKeyDown={handleKeyDown}
//                   // onKeyPress={handleKeyDown}
//                 />
// {/*             <TextField
//               className={classes.textField}
//               label="Código de acceso"
//               name="otpCode"
//               onChange={event =>
//                 this.handleFieldChange('otpCode', event.target.value)
//               }
//               type="number"
//               //value={values.otpCode}
//               variant="outlined"
//             /> */}
//           </form>
//         </PortletContent>
//         <PortletFooter className={classes.portletFooter}>
//         <Button
//             style={{ marginLeft: '30px' }}
//             color="primary"
//             variant="outlined"
//             onClick={functionClick}>
//               Buscar
//         </Button>
//         </PortletFooter>
//       </Portlet>



  );
};

export default withStyles(styles)(PatientSearch);
