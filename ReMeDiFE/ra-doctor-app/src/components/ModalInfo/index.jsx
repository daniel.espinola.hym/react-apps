import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import ButtonBootstrap from 'react-bootstrap/Button';
const ModalInfo = ({ message }) => {
  const [show, setShow] = useState(true);
  const handleClose = () => setShow(false);
  return (
    <Modal centered show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Atencion</Modal.Title>
      </Modal.Header>
      <Modal.Body>{message}</Modal.Body>
      <Modal.Footer>
        <ButtonBootstrap variant="secondary" onClick={handleClose}>
          Cerrar
        </ButtonBootstrap>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalInfo;
