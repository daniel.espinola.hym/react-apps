import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import classNames from 'classnames';
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent
} from 'components';

const styles = theme => ({
  root: {},
  form: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  group: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2
  },
  groupLabel: {
    paddingLeft: theme.spacing.unit * 2
  },
  field: {
    marginBottom: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center'
  },
  textField: {
    width: '320px',
    maxWidth: '100%',
    marginRight: theme.spacing.unit * 3
  }
});

const DoctorInfo = ({
  name,
  hospital,
  license,
  classes,
  className,
  ...rest
}) => {
  const rootClassName = classNames(classes.root, className);
  return (
    <Portlet {...rest} className={rootClassName}>
      <PortletHeader>
        <PortletLabel title="Información del doctor" />
      </PortletHeader>
      <PortletContent noPadding>
        <form className={classes.form}>
          <div className={classes.group}>
            <div className={classes.field}>
              <div>
                <Typography style={{ fontWeight: 'bold' }} variant="body1">
                  Nombre:
                </Typography>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <Typography style={{ fontWeight: 'bold' }} variant="body1">
                  Matrícula:
                </Typography>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <Typography style={{ fontWeight: 'bold' }} variant="body1">
                  Hospital:
                </Typography>
              </div>
            </div>
          </div>
          <div className={classes.group}>
            <div className={classes.field}>
              <div>
                <Typography variant="body1">{name}</Typography>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <Typography variant="body1">{license}</Typography>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <Typography variant="body1">{hospital}</Typography>
              </div>
            </div>
          </div>
        </form>
      </PortletContent>
    </Portlet>
  );
};

export default withStyles(styles)(DoctorInfo);
