import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

// Externals
import classNames from 'classnames';
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

//api
import { getDoctorInformation } from 'api/doctor';

// Material components
import {
  Avatar,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography
} from '@material-ui/core';

// Material icons
import {
  DashboardOutlined as DashboardIcon,
  PeopleOutlined as PeopleIcon,
  WatchLater as WatchIcon,
  ShoppingBasketOutlined as ShoppingBasketIcon,
  InfoOutlined as InfoIcon,
  AccountBoxOutlined as AccountBoxIcon,
  SettingsOutlined as SettingsIcon,
  ContactMailOutlined,
  AirlineSeatIndividualSuiteOutlined,
  Assignment
} from '@material-ui/icons';

// Component styles
import styles from './styles';

class Sidebar extends Component {
  constructor() {
    super();

    this.state = {
      doctorInfo: {}
    };
  }

  async componentDidMount() {
    //Voy a buscar la informacion del doctor que esta logeado
    const doctorInfo = await getDoctorInformation();
    this.setState({
      doctorInfo
    });
  }
  render() {
    const { classes, className } = this.props;

    const rootClassName = classNames(classes.root, className);

    return (
      <nav className={rootClassName}>
        <div className={classes.logoWrapper}>
          <Link className={classes.logoLink} to="/medicalConsultation">
            <img
              alt="ReMeDi"
              className={classes.remediIcon}
              src="/images/logos/logo-remedi.png"
              style={{ width: '70px;' }}
            />
          </Link>
        </div>
        <Divider className={classes.logoDivider} />
        <div className={classes.profile}>
          <Avatar
            alt="Roman Kutepov"
            className={classes.avatar}
            src="/images/avatars/medico.png"
          />
          <Typography className={classes.nameText} variant="h6">
            {this.state.doctorInfo.name}
          </Typography>
          <Typography className={classes.bioText} variant="caption">
            {this.state.doctorInfo.hospital}
          </Typography>
        </div>
        <Divider className={classes.profileDivider} />
        <List component="div" disablePadding>
          <ListItem
            activeClassName={classes.activeListItem}
            className={classes.listItem}
            component={NavLink}
            to="/medicalConsultation">
            <ListItemIcon className={classes.listItemIcon}>
              <ContactMailOutlined />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Nueva receta"
            />
          </ListItem>
          <ListItem
            activeClassName={classes.activeListItem}
            className={classes.listItem}
            component={NavLink}
            to="/chronicDiseaseAssignment">
            <ListItemIcon className={classes.listItemIcon}>
              <AirlineSeatIndividualSuiteOutlined />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Diagnósticos crónicos"
            />
          </ListItem>
          <ListItem
            activeClassName={classes.activeListItem}
            className={classes.listItem}
            component={NavLink}
            to="/chronicAsignament">
            <ListItemIcon className={classes.listItemIcon}>
              <WatchIcon />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Asignación crónica"
            />
          </ListItem>
          <ListItem
            activeClassName={classes.activeListItem}
            className={classes.listItem}
            component={NavLink}
            to="/patientList">
            <ListItemIcon className={classes.listItemIcon}>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Consultas pacientes crónicos"
            />
          </ListItem>
          <ListItem
            activeClassName={classes.activeListItem}
            className={classes.listItem}
            component={NavLink}
            to="/prescriptions">
            <ListItemIcon className={classes.listItemIcon}>
              <Assignment />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Recetas pendientes de firma"
            />
          </ListItem>
        </List>
      </nav>
    );
  }
}

Sidebar.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Sidebar);
