import React, { Component } from 'react';
import { Router } from 'react-router-dom';

import history from './common/utils/history';

// Externals
import { Chart } from 'react-chartjs-2';

// Material helpers
import { ThemeProvider } from '@material-ui/styles';

// ChartJS helpers
import { chartjs } from './utils';

// Theme
import theme from './theme';

// Styles
import 'react-perfect-scrollbar/dist/css/styles.css';
import './assets/scss/index.scss';

// Routes
import Routes from './Routes';

import AuthenticationService from 'common/services/AuthenticationService';

import Modal from 'react-bootstrap/Modal';

import ButtonBootstrap from 'react-bootstrap/Button';

// Configure ChartJS
Chart.helpers.extend(Chart.elements.Rectangle.prototype, {
  draw: chartjs.draw
});

const minutesForInactivity = 20;

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logginStatus: true,
      showModal: false,
      modalMsg: ''
    };
    this.events = [
      'load',
      'mousemove',
      'mousedown',
      'click',
      'scroll',
      'keypress'
    ];

    //this.warn = this.warn.bind(this);
    this.logout = this.logout.bind(this);
    this.resetTimeout = this.resetTimeout.bind(this);

    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }

    this.setTimeout();
  }

  clearTimeout() {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  }

  setTimeout() {
    this.logoutTimeout = setTimeout(this.logout, minutesForInactivity * 60000);
  }

  resetTimeout() {
    this.clearTimeout();
    this.setTimeout();
  }

  logout() {
    // this.setState({ logginStatus: false });
    // this.destroy(); // Cleanup
    AuthenticationService.logout();
    this.setState({ showModal: true });
    this.setState({ modalMsg: 'Se ha cerrado la sesión por inactividad.' });
    //alert("Se ha cerrado la sesión por inactividad.");
  }

  handleClose = () => {
    this.setState({ showModal: false });
    history.push('/login');
  };

  destroy() {
    this.clearTimeout();

    for (var i in this.events) {
      window.removeEventListener(this.events[i], this.resetTimeout);
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <Routes />
        </Router>

        <Modal centered show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Error</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.state.modalMsg}</Modal.Body>
          <Modal.Footer>
            <ButtonBootstrap variant="secondary" onClick={this.handleClose}>
              Cerrar
            </ButtonBootstrap>
          </Modal.Footer>
        </Modal>
      </ThemeProvider>
    );
  }
}
