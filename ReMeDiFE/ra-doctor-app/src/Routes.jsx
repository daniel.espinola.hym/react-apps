import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// Views
import Dashboard from './views/Dashboard';
import ProductList from './views/ProductList';
import UserList from './views/UserList';
import Account from './views/Account';
import Settings from './views/Settings';
import NotFound from './views/NotFound';
import DrugList from './views/MedicalConsultation/Prescription/DrugList';
import MedicalConsultation from './views/MedicalConsultation';
import ChronicDiseaseAssignment from './views/ChronicDiseaseAssignment';
import ChronicAsignament from './views/ChronicAsignament';
import PrescriptionPending from './views/PrescriptionPending';
import PatientList from './views/PatientList';
import LoginPage from './views/LoginPage';
import AuthenticatedRoute from './components/AuthenticatedRoute/AuthenticatedRoute';

export default class Routes extends Component {
  render() {
    return (
      <Switch>
        <Redirect exact from="/" to="/login" />
        <Route component={LoginPage} exact path="/login" />
        <AuthenticatedRoute component={Dashboard} exact path="/dashboard" />
        <AuthenticatedRoute component={PatientList} exact path="/patientList" />
        <AuthenticatedRoute component={UserList} exact path="/users" />
        <AuthenticatedRoute component={DrugList} exact path="/drugs" />
        <AuthenticatedRoute component={ProductList} exact path="/products" />
        <AuthenticatedRoute component={Account} exact path="/account" />
        <AuthenticatedRoute component={Settings} exact path="/settings" />
        <AuthenticatedRoute
          component={MedicalConsultation}
          exact
          path="/medicalConsultation"
        />
        <AuthenticatedRoute
          component={ChronicDiseaseAssignment}
          exact
          path="/chronicDiseaseAssignment"
        />
        <AuthenticatedRoute
          component={PrescriptionPending}
          exact
          path="/prescriptions"
        />
        <AuthenticatedRoute
          component={ChronicAsignament}
          exact
          path="/chronicAsignament"
        />
        <Route component={NotFound} exact path="/not-found" />
        <Redirect to="/not-found" />
      </Switch>
    );
  }
}
