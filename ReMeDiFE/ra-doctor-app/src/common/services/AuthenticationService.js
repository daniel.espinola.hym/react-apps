import axiosInstance from 'api/api';

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';

class AuthenticationService {
  executeJwtAuthenticationService(username, password, idHospital) {
    console.log(username);
    return axiosInstance.post(`authenticateDoctor`, {
      username,
      password,
      idHospital
    });
  }

  executeJwtPreAuthenticationService(username, password) {
    console.log(username);
    return axiosInstance.post(`preAuthenticateDoctor`, {
      username,
      password
    });
  }

  registerSuccessfulLoginForJwt(username, token) {
    sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
    var jwtDecode = require('jwt-decode');
    let decoded = jwtDecode(token);
    sessionStorage.setItem('dni', decoded.dni);
    sessionStorage.setItem('fullName', decoded.fullName);
    sessionStorage.setItem('jwtToken', token);
    sessionStorage.setItem('licenseNumber', decoded.licenseNumber);
    sessionStorage.setItem('hospitalName', decoded.hospitalName);
    sessionStorage.setItem('idHospital', decoded.idHospital);
    sessionStorage.setItem('idDoctor', decoded.idDoctor);
    sessionStorage.setItem('id', decoded.idDoctor);
  }

  logout() {
    sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    sessionStorage.removeItem('jwtToken');
    sessionStorage.removeItem('dni');
    sessionStorage.removeItem('fullName');
    sessionStorage.removeItem('licenseNumber');
    sessionStorage.removeItem('idHospital');
    sessionStorage.removeItem('hospitalName');
    sessionStorage.removeItem('id');
    sessionStorage.removeItem('idDoctor');
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) return false;
    return true;
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) return '';
    return user;
  }

  getDni() {
    let dni = sessionStorage.getItem('dni');
    if (dni === null) return '';
    return dni;
  }

  getIdHospital() {
    let idHospital = sessionStorage.getItem('idHospital');
    if (idHospital === null) return '';
    return idHospital;
  }

  getHospitalName() {
    let hospitalName = sessionStorage.getItem('hospitalName');
    if (hospitalName === null) return '';
    return hospitalName;
  }

  getIdDoctor() {
    let idDoctor = sessionStorage.getItem('idDoctor');
    if (idDoctor === null) return '';
    return idDoctor;
  }

  getDoctorName() {
    let fullName = sessionStorage.getItem('fullName');
    if (fullName === null) return '';
    return fullName;
  }

  getDoctorLicense() {
    let licenseNumber = sessionStorage.getItem('licenseNumber');
    if (licenseNumber === null) return '';
    return licenseNumber;
  }
}

export default new AuthenticationService();
