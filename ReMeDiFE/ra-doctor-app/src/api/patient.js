import API from './api';
import AuthenticationService from 'common/services/AuthenticationService';

export const getPatientByDni = dni => {
  return API.get('patient/' + dni + '?idHospital=' + AuthenticationService.getIdHospital());
};
