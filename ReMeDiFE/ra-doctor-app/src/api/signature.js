import API from './apisignature';

const headers = {
  'Content-Type': 'application/json;charset=UTF-8'
};

export const generatePrescriptionWithSignature = prescription => {
  return API.post('sign', JSON.stringify({ prescriptionRequest: prescription }), {
    headers: headers
  });
};
