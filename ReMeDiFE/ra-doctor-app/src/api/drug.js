import API from './api';

export const getAllDrugsBack = () => {
  return API.get('drug');
};

export const getDrugData = drugId => {
  return API.get('drug/' + drugId + '/options');
};

export const getDrugListByPrescriptionId = prescriptionId => {
  return API.get('drug/prescription/' + prescriptionId);
};
