import API from './api';
import AuthenticationService from 'common/services/AuthenticationService';

export const getDoctorInformation = () => {
  return {
    license: AuthenticationService.getDoctorLicense(),
    name: AuthenticationService.getDoctorName(),
    hospital: AuthenticationService.getHospitalName(),
    id: AuthenticationService.getIdDoctor(),
    hospitalId: AuthenticationService.getIdHospital()
  };
};

export const getOwnChronicPatientList = () => {
  return API.get(
    '/doctor/getChronicPatientList/' +
      AuthenticationService.getIdDoctor() +
      '/' +
      AuthenticationService.getIdHospital()
  );
};
