import axios from 'axios';
import AuthenticationService from '../common/services/AuthenticationService';
import history from 'common/utils/history';

const axiosInstance = axios.create({
  baseURL:
    'https://api.remedi.com.ar/recetasunlam/'
  //'http://localhost:8080/recetasunlam/'
});

const defaults = {
  /** @type {Number[]} */
  statusCodes: [
    401 // Unauthorized
  ]
};

axiosInstance.interceptors.request.use(function (config) {
  if (!AuthenticationService.isUserLoggedIn()) {
    history.push('/login');
  }
  const token = sessionStorage.getItem('jwtToken');
  config.headers.Authorization = token ? `Bearer ${token}` : '';

  return config;
});

const refreshAuthLogic = failedRequest =>
  axiosInstance.post('refresh').then(tokenRefreshResponse => {
    sessionStorage.setItem('jwtToken', tokenRefreshResponse.data.token);
    failedRequest.response.config.headers.Authorization = tokenRefreshResponse
      .data.token
      ? `Bearer ${tokenRefreshResponse.data.token}`
      : '';
    return Promise.resolve();
  });
//const axiosInstance = createAuthRefreshInterceptor(axiosInstance, refreshAuthLogic);
function createAuthRefreshInterceptor(axios, refreshTokenCall, options = {}) {
  const id = axiosInstance.interceptors.response.use(
    response => response,
    error => {
      //debugger;
      // Reject promise if the error status is not in options.ports or defaults.ports
      console.log(error);
      //console.log(res);
      const statusCodes =
        options.hasOwnProperty('statusCodes') && options.statusCodes.length
          ? options.statusCodes
          : defaults.statusCodes;

      if (!error.response) {
        alert('Error de conexion con el servidor');
      }

      if (
        !error.response ||
        (error.response.status &&
          statusCodes.indexOf(+error.response.status) === -1)
      ) {
        return Promise.reject(error);
      }

      if (
        error.response.status &&
        statusCodes.indexOf(+error.response.status) != -1 &&
        error.response.headers.cantokenberefreshed == 'false'
      ) {
        AuthenticationService.logout();
        history.push('/login');
        return Promise.reject(error);
      }

      // Remove the interceptor to prevent a loop
      // in case token refresh also causes the 401
      axiosInstance.interceptors.response.eject(id);

      const refreshCall = refreshTokenCall(error);

      // Create interceptor that will bind all the others requests
      // until refreshTokenCall is resolved
      const requestQueueInterceptorId = axiosInstance.interceptors.request.use(
        request => refreshCall.then(() => request)
      );

      // When response code is 401 (Unauthorized), try to refresh the token.
      return refreshCall
        .then(() => {
          axiosInstance.interceptors.request.eject(requestQueueInterceptorId);
          return axiosInstance(error.response.config);
        })
        .catch(error => {
          axiosInstance.interceptors.request.eject(requestQueueInterceptorId);
          return Promise.reject(error);
        })
        .finally(() =>
          createAuthRefreshInterceptor(axiosInstance, refreshTokenCall, options)
        );
    }
  );
}

createAuthRefreshInterceptor(axiosInstance, refreshAuthLogic, {
  statusCodes: [401]
});

export default axiosInstance;
