import API from './api';

export const getAllDiagnosis = () => {
  return API.get('diagnosis');
};

export const getAllChronicDiagnosis = () => {
  return API.get('diagnosis/chronic');
};

export const getAllChronicDiagnosisByPatientId = patientId => {
  return API.get('diagnosis/chronic/patient/' + patientId);
};
