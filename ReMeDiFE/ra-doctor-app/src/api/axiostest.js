import axios from 'axios';

export const getAxios = () => {
  return axios.get(`http://localhost:8080/recetasunlam/getDiagnosis`);
};

export const getAxios2 = () => {
  return axios.get(`http://localhost:8080/recetasunlam/getDiagnosis2`);
};
