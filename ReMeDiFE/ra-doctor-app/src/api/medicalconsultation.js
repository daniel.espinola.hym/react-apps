import API from './api';
import AuthenticationService from 'common/services/AuthenticationService';

export const postPrescription = prescription => {
  return API.post('/medicalConsultation/prescription', prescription);
};

export const signPrescription = (prescriptionId, prescriptionFile) => {
  return API.post('/medicalConsultation/prescription/' + prescriptionId, prescriptionFile);
};

export const postChronicDiseaseToPatient = status => {
  return API.post('/medicalConsultation/prescription/patientStatusChronic', status);
};

export const postChronicAssignment = chronicAssignment => {
  return API.post('/medicalConsultation/prescription/chronic', chronicAssignment);
};

export const getPrescriptionSigned = prescriptionId => {
  return API.get('/medicalConsultation/prescription/' + prescriptionId);
};

export const getPrescriptionPreview = (patientId, medicalConsultationId) => {
  return API.get('/patient/medicalConsultation/' + medicalConsultationId);
};

export const getChronicMedicalConsultation = patientId => {
  return API.get('/medicalConsultation/chronic/' + AuthenticationService.getIdHospital() + '/' + patientId);
};

export const getChronicAssignmentByPatientId = patientId => {
  return API.get('/medicalConsultation/prescription/chronic/' + patientId);
};

export const getPrescriptionPendingByDoctorId = () => {
  return API.get('/medicalConsultation/prescription/pending/' + AuthenticationService.getIdDoctor());
};
